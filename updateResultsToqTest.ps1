$filePath="target\surefire-reports\TEST-sample.junit.CalculateTest.xml"
$accessToken="0606c2d9-65cd-4bea-a02f-d2b4099d025f"
$requestUri="https://pulse-8.qtestnet.com/webhook/d61267c7-0fea-40ec-958c-709ca5a7c9dc"
$projectId=23363
$testcycle=219668
$body = [PSCustomObject] @{
  projectId=$projectId;
  testcycle=$testcycle;
  result=[Convert]::ToBase64String((Get-Content -path $filePath -Encoding byte));
} | ConvertTo-Json
Invoke-RestMethod -Method POST -Headers @{Authorization="Bearer $accessToken"} -Uri $requestUri -ContentType application/json -body $body

$filePath="target\surefire-reports\TEST-sample.junit.CalculatorTestSuccessful.xml"
$accessToken="0606c2d9-65cd-4bea-a02f-d2b4099d025f"
$requestUri="https://pulse-8.qtestnet.com/webhook/d61267c7-0fea-40ec-958c-709ca5a7c9dc"
$projectId=23363
$testcycle=219668
$body = [PSCustomObject] @{
  projectId=$projectId;
  testcycle=$testcycle;
  result=[Convert]::ToBase64String((Get-Content -path $filePath -Encoding byte));
} | ConvertTo-Json
Invoke-RestMethod -Method POST -Headers @{Authorization="Bearer $accessToken"} -Uri $requestUri -ContentType application/json -body $body

$filePath="target\surefire-reports\TEST-sample.junit.HelloWord.xml"
$accessToken="0606c2d9-65cd-4bea-a02f-d2b4099d025f"
$requestUri="https://pulse-8.qtestnet.com/webhook/d61267c7-0fea-40ec-958c-709ca5a7c9dc"
$projectId=23363
$testcycle=219668
$body = [PSCustomObject] @{
  projectId=$projectId;
  testcycle=$testcycle;
  result=[Convert]::ToBase64String((Get-Content -path $filePath -Encoding byte));
} | ConvertTo-Json
Invoke-RestMethod -Method POST -Headers @{Authorization="Bearer $accessToken"} -Uri $requestUri -ContentType application/json -body $body
